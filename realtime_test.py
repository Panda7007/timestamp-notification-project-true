# Untuk capture dan deteksi
import cv2
import numpy as np
import time
import datetime as dt
import os
import imageio
import requests

# Mengambil gambar dari sumber
cap = cv2.VideoCapture(0)

# Mendefinisikan path lengkap atau relatif ke file model ONNX
nama_onnx = input("Masukkan nama model: ")
model_path = os.path.join("model", f"{nama_onnx}.onnx")

# Memuat model ONNX
net = cv2.dnn.readNetFromONNX(model_path)

# Buat menandai class berdasarkan urutan class di roboflow
classes = [nama_onnx]
tanggal = dt.datetime.now().date()

# Define the codec and create VideoWriter object
fpsvideo = 5
output_folder = "video"
if not os.path.exists(output_folder):
    os.makedirs(output_folder)
output_video_path = os.path.join(output_folder, f'CCTV-{tanggal}.mp4')

# Buat objek writer dengan menggunakan ImageIO
out = imageio.get_writer(output_video_path, fps=fpsvideo)

# settingan untuk melihat FPS
starting_time = time.time()
frame_id = 0
font = cv2.FONT_HERSHEY_COMPLEX

# Mendefinisikan path file output di dalam folder khusus
output_file = f"CCTV-{nama_onnx}-{str(tanggal)}-timestamp.txt"
output_file = os.path.join("timestamp", output_file)

# Membuat folder bukti_foto jika belum ada
bukti_foto_folder = "bukti_foto"
if not os.path.exists(bukti_foto_folder):
    os.makedirs(bukti_foto_folder)

# Mendefinisikan selisih timelapse pertama dengan selanjutnya
detik = 30
frame_count = 0
detected_frames = []

# untuk kondisional alaram
jam2 = 0
jam3 = 19


# Text caption to be sent along with the photo
caption_text = "Terdeteksi gerakan di kamera"

# Ganti 'URL_TARGET' dengan URL yang sesuai
data = {'chat_id': '-4062304369', 'caption': caption_text}

while True:
    img = cap.read()[1]
    frm = img.copy()
    frame_id += 1
    if img is None:
        break
    img = cv2.resize(img, (500, 300))
    blob = cv2.dnn.blobFromImage(img, scalefactor=1/255, size=(640, 640), mean=[0, 0, 0], swapRB=True, crop=False)
    net.setInput(blob)
    detections = net.forward()[0]

    classes_ids = []
    confidences = []
    boxes = []
    rows = detections.shape[0]

    img_width, img_height = img.shape[1], img.shape[0]
    x_scale = img_width / 640
    y_scale = img_height / 640

    for i in range(rows):
        row = detections[i]
        confidence = row[4]
        if confidence >= 0.25:
            classes_score = row[5:]
            ind = np.argmax(classes_score)
            if classes_score[ind] >= 0.25:
                classes_ids.append(ind)
                confidences.append(confidence)
                cx, cy, w, h = row[:4]
                x1 = int((cx - w / 2) * x_scale)
                y1 = int((cy - h / 2) * y_scale)
                width = int(w * x_scale)
                height = int(h * y_scale)
                box = np.array([x1, y1, width, height])
                boxes.append(box)

    indices = cv2.dnn.NMSBoxes(boxes, confidences, 0.25, 0.25)

    waktu = time.asctime()
    jam = dt.datetime.now().hour
    tanggal = dt.datetime.now().date()

    elapsed_time = time.time() - starting_time
    fps = frame_id / elapsed_time

    cv2.putText(img, "Jumlah Orang : " + str(len(indices)), (10, 90), font, 0.4, (200, 0, 0), 1)
    cv2.putText(img, "Fps : " + str(fps), (10, 30), font, 0.3, (255, 0, 0), 1)
    cv2.putText(img, "Waktu : " + str(waktu), (10, 50), font, 0.3, (200, 0, 0), 1)
    cv2.putText(img, "Tekan Q untuk keluar & Save ", (10, 280), font, 0.3, (200, 200, 200), 1)
    cv2.putText(img, "Jam : " + str(jam), (10, 70), font, 0.3, (200, 0, 0), 1)

    # mengaturjarak antar timestamp dengan mengalikan FPS
    selisih = int(fps * detik)

    pos_frame = cap.get(cv2.CAP_PROP_POS_FRAMES)

    # Boxing object yang terdeteksi
    for i in indices:
        x1, y1, w, h = boxes[i]
        label = classes[classes_ids[i]]
        conf = confidences[i]
        text = label + "{:.2f}".format(conf)
        cv2.rectangle(img, (x1, y1), (x1 + w, y1 + h), (255, 0, 0), 2)
        cv2.putText(img, text, (x1, y1 - 2), cv2.FONT_HERSHEY_COMPLEX, 0.3, (0, 0, 255), 2)
        cv2.rectangle(frm, (x1, y1), (x1 + w, y1 + h), (255, 0, 0), 2)
        cv2.putText(frm, text, (x1, y1 - 2), cv2.FONT_HERSHEY_COMPLEX, 0.3, (0, 0, 255), 2)
        detected_frames.append(pos_frame)

    out.append_data(frm)
    print("Posisi frame saat ini:", frame_id)
    cv2.imshow("VIDEO", img)

    # Path folder untuk menyimpan bukti terdeteksi
    output_folder = "bukti_terdeteksi"

    # Deteksi manusia untuk menjalankan alarm
    if len(indices) > 0:
        # Kode untuk mengirimkan alarm dengan email
        if jam < 7:
            if jam2 < jam:
                jam2 = jam2 + 1
            if jam2 == jam:
                # Menyimpan gambar ke dalam folder bukti_foto
                cv2.imwrite(os.path.join(bukti_foto_folder, f'{tanggal}-Jam-{jam}-gambar.png'), img)
                namafile = os.path.join(bukti_foto_folder, f'{tanggal}-Jam-{jam}-gambar.png')
                # Open the file again before sending the request
                with open(namafile, 'rb') as file:
                    files = {'photo': file}
                    response = requests.post(
                        'https://api.telegram.org/bot6781633761:AAGw1NGbm1lsBFfNXQ8yxfw5IGG_OIFZcpA/sendPhoto',
                        files=files, data=data)
                print("Email terkirim yaak")
                jam2 = jam2 + 1
        elif jam > 18:
            if jam3 < jam:
                jam3 = jam3 + 1
            if jam3 == jam:
                # Menyimpan gambar ke dalam folder bukti_foto
                cv2.imwrite(os.path.join(bukti_foto_folder, f'{tanggal}-Jam-{jam}-gambar.png'), img)
                namafile = os.path.join(bukti_foto_folder, f'{tanggal}-Jam-{jam}-gambar.png')
                # Open the file again before sending the request
                with open(namafile, 'rb') as file:
                    files = {'photo': file}
                    response = requests.post(
                        'https://api.telegram.org/bot6781633761:AAGw1NGbm1lsBFfNXQ8yxfw5IGG_OIFZcpA/sendPhoto',
                        files=files, data=data)
                print("Email terkirim yaak")
                jam3 = jam3 + 1

    # Reset jam2 dan jam3 jika jam == 0
    if jam == 0:
        jam2 = 0
        jam3 = 19

    # menulis tiemstamp
    frame_count += 1
    if frame_count == selisih:
        # Simpan nomor frame yang terdeteksi ke dalam file teks
        with open(output_file, 'a') as file:
            file.write(str(int(frame_id)) + '\n')
        print("Posisi frame yang tercatat dalam database:", int(frame_id))
        detected_frames = []
        frame_count = 0

    k = cv2.waitKey(10)
    if k == ord('q'):
        break

    #Untuk uji coba email notification
    menit= 60
    #mengaturjarak antar timestamp dengan mengalikan FPS
    selisih_notification = int(fps * 2 * menit)


    frame_count_test+= 1
    if frame_count_test == selisih_notification:
        # Menyimpan gambar ke dalam folder bukti_foto
        cv2.imwrite(os.path.join(bukti_foto_folder, f'{tanggal}-Jam-{jam}-gambar.png'), img)
        namafile = os.path.join(bukti_foto_folder, f'{tanggal}-Jam-{jam}-gambar.png')
        # Open the file again before sending the request
        with open(namafile, 'rb') as file:
            files = {'photo': file}
            response = requests.post(
                'https://api.telegram.org/bot6781633761:AAGw1NGbm1lsBFfNXQ8yxfw5IGG_OIFZcpA/sendPhoto',
                files=files, data=data)
        print("Email terkirim yaak")
        frame_count_test=0

# Release the capture and writer objects
cap.release()

# Destroy all windows
cv2.destroyAllWindows()

# Release the writer object
out.close()

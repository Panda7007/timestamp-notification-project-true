import cv2
import os
from pynput import keyboard

#Khusus MAC OS


# Mengambil gambar dari sumber
video_name = input("Video name: ")
video_path = os.path.join("video", video_name + ".mp4")
video = cv2.VideoCapture(video_path)

# Membaca angka-angka menit dari file teks
file_time = input("Nama timestamp: ")
file_model_path = os.path.join("timestamp", video_name + "_" + file_time + "_timestamp.txt")
with open(file_model_path, 'r') as file:
    menit = [int(line.strip()) for line in file]

memori = 0  # Memori awal diatur ke 0

frame_rate = video.get(cv2.CAP_PROP_FPS)
target_frame = 1  # Konversi menit menjadi frame
video.set(cv2.CAP_PROP_POS_FRAMES, int(target_frame))

paused = False
slow_motion = False
delay = int(1000 / frame_rate)  # Delay untuk frame berikutnya

screenshot_count = 0


def on_press(key):
    global memori, paused, slow_motion, delay, screenshot_count

    if key == keyboard.Key.esc:
        return False  # Stop listener

    # Memutar mundur video ketika tombol 'a' ditekan
    if key == keyboard.KeyCode.from_char('a'):
        if memori > 0:
            memori -= 1
            target_frame = menit[memori]
            video.set(cv2.CAP_PROP_POS_FRAMES, int(target_frame))

    # Memutar maju video ketika tombol 'd' ditekan
    elif key == keyboard.KeyCode.from_char('d'):
        if memori < len(menit) - 1:
            memori += 1
            target_frame = menit[memori]
            video.set(cv2.CAP_PROP_POS_FRAMES, int(target_frame))

    # Mengaktifkan atau menonaktifkan mode pause saat tombol 'p' ditekan
    elif key == keyboard.KeyCode.from_char('p'):
        paused = not paused
        if paused:
            delay = 0  # Menghentikan delay untuk frame berikutnya
        else:
            delay = int(1000 / frame_rate)  # Mengembalikan delay dengan sesuai frame rate

    # Mengaktifkan atau menonaktifkan mode slow motion saat tombol 's' ditekan
    elif key == keyboard.KeyCode.from_char('s'):
        slow_motion = not slow_motion
        if slow_motion:
            delay *= 10  # Memperlambat delay menjadi 10 kali lipat
        else:
            delay = int(1000 / frame_rate)  # Mengembalikan delay dengan sesuai frame rate

    elif key == keyboard.KeyCode.from_char('o'):
        screenshot_count += 1
        screenshot_name = f'screenshot_{screenshot_count}.png'
        cv2.imwrite(screenshot_name, frame)
        print(f"Tangkapan layar berhasil disimpan sebagai {screenshot_name}")


# Listener untuk menangkap input keyboard
listener = keyboard.Listener(on_press=on_press)
listener.start()

while True:
    # Membaca frame demi frame
    ret, frame = video.read()

    # Memastikan video telah terbaca dengan baik
    if not ret:
        break

    frame = cv2.resize(frame, (500, 300))

    cv2.putText(frame, "Fps: " + str(frame_rate), (10, 70), cv2.FONT_HERSHEY_COMPLEX, 0.3, (200, 0, 0), 1)
    cv2.putText(frame, "a = Sebelumnya | d = selanjutnya ", (10, 80), cv2.FONT_HERSHEY_COMPLEX, 0.3, (200, 0, 0), 1)
    cv2.putText(frame, "p = Pause | s = slowmotion ", (10, 90), cv2.FONT_HERSHEY_COMPLEX, 0.3, (200, 0, 0), 1)
    cv2.putText(frame, "o = Screenshoot | q = quit ", (10, 100), cv2.FONT_HERSHEY_COMPLEX, 0.3, (200, 0, 0), 1)

    # Menampilkan frame yang dibaca
    cv2.imshow('Video', frame)

    # Menghentikan loop jika tombol 'q' ditekan
    if cv2.waitKey(delay) & 0xFF == ord('q'):
        break

# Menutup video dan menghapus jendela tampilan
video.release()
cv2.destroyAllWindows()

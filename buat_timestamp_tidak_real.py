import os
import cv2
import numpy as np
import time
import datetime as dt
import imageio

# Mengambil gambar dari sumber
video_name = input("Video name: ")
video_path = os.path.join("input", video_name + ".mp4")
cap = cv2.VideoCapture(video_path)

# Mendefinisikan path lengkap atau relatif ke file model ONNX
nama_onnx = input("Masukkan nama model: ")
model_path = os.path.join("model", nama_onnx + ".onnx")

# Memuat model ONNX
net = cv2.dnn.readNetFromONNX(model_path)

# Buat menandai class berdasarkan urutan class di roboflow
classes = [nama_onnx + " : "]

# Mendefinisikan selisih timelapse pertama dengan selanjutnya
frame_rate = cap.get(cv2.CAP_PROP_FPS)
detik = 10
selisih = int(frame_rate * detik)

tanggal = dt.datetime.now().date()

# Settingan untuk melihat FPS
starting_time = time.time()
frame_id = 0
font = cv2.FONT_HERSHEY_COMPLEX

frame_count = 0
detected_frames = []

# Mendefinisikan path file output di dalam folder khusus
output_file = video_name + "-" + nama_onnx +"-"+str(tanggal)+ "-timestamp.txt"
output_file = os.path.join("timestamp", output_file)

# Mendefinisikan path untuk menyimpan hasil video
output_folder = "video"
if not os.path.exists(output_folder):
    os.makedirs(output_folder)

output_video_path = os.path.join(output_folder, video_name +"-"+ str(tanggal) + ".mp4")

# Membuat objek writer menggunakan imageio
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
writer = imageio.get_writer(output_video_path, fps=frame_rate, macro_block_size=None)

while True:
    ret, img = cap.read()
    if not ret:
        break

    frm = img.copy()
    frame_id += 1

    img = cv2.resize(img, (frame_width, frame_height))  # Menyesuaikan ukuran frame
    blob = cv2.dnn.blobFromImage(img, scalefactor=1/255, size=(640, 640), mean=[0, 0, 0], swapRB=True, crop=False)
    net.setInput(blob)
    detections = net.forward()[0]

    waktu = time.asctime()
    jam = dt.datetime.now().hour
    

    elapsed_time = time.time() - starting_time
    fps = frame_id / elapsed_time

    classes_ids = []
    confidences = []
    boxes = []
    rows = detections.shape[0]

    img_width, img_height = img.shape[1], img.shape[0]
    x_scale = img_width / 640
    y_scale = img_height / 640

    for i in range(rows):
        row = detections[i]
        confidence = row[4]
        if confidence >= 0.25:
            classes_score = row[5:]
            ind = np.argmax(classes_score)
            if classes_score[ind] >= 0.25:
                classes_ids.append(ind)
                confidences.append(confidence)
                cx, cy, w, h = row[:4]
                x1 = int((cx - w / 2) * x_scale)
                y1 = int((cy - h / 2) * y_scale)
                width = int(w * x_scale)
                height = int(h * y_scale)
                box = np.array([x1, y1, width, height])
                boxes.append(box)

    indices = cv2.dnn.NMSBoxes(boxes, confidences, 0.25, 0.25)
    cv2.putText(img, "Jumlah Orang : " + str(len(indices)), (10, 40), font, 0.6, (200, 0, 0), 2)
    cv2.putText(img, "Fps : " + str(fps), (10, 60), font, 0.6, (255, 0, 0), 1)
    cv2.putText(img, "Waktu : " + str(waktu), (10, 80), font, 0.6, (200, 0, 0), 1)
    cv2.putText(img, "Tekan Q untuk keluar & Save ", (10, 580), font, 0.6, (200, 200, 200), 1)

    pos_frame = cap.get(cv2.CAP_PROP_POS_FRAMES)

    # Boxing object yang terdeteksi
    for i in indices:
        x1, y1, w, h = boxes[i]
        label = classes[classes_ids[i]]
        conf = confidences[i]
        text = label + "{:.2f}".format(conf)
        cv2.rectangle(img, (x1, y1), (x1 + w, y1 + h), (255, 0, 0), 2)
        cv2.putText(img, text, (x1, y1 - 2), cv2.FONT_HERSHEY_COMPLEX, 0.3, (0, 0, 255), 2)
        detected_frames.append(pos_frame)

    # Menyimpan frame ke dalam video hasil
    writer.append_data(img)

    cv2.imshow("VIDEO", img)
    print("Posisi frame saat ini:", pos_frame)

    frame_count += 1
    if frame_count == selisih:
        # Simpan nomor frame yang terdeteksi ke dalam file teks
        with open(output_file, 'a') as file:
            file.write(str(int(pos_frame)) + '\n')
        print("Posisi frame yang tercatat dalam database:", int(pos_frame))
        detected_frames = []
        frame_count = 0

    k = cv2.waitKey(10)
    if k == ord('q'):
        break

# Release the capture, writer, and destroy all windows
cap.release()
writer.close()
cv2.destroyAllWindows()
